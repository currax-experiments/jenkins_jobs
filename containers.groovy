// DESCRIPTION: This script creates the containters folder and all of the container build jobs inside of the folder. 

folder('Containers')

pipelineJob('Containers/vault') {
    displayName('Vault Container')
    description('\n This Job builds Vault containers. \n --- This job is managed via JobDSL. --- \n --- DO NOT MANULLY MODIFY. ---')
    definition {
        cpsScm{
            lightweight(true)
            scm{
                git{
                    remote{
                        url('git@gitlab.com:currax_containers/vault.git')
                        credentials('curraxn_gitlab')
                    }
                    extensions{
                        wipeOutWorkspace()
                    }
                }
            }
            triggers {
                scm('H/5 * * * *')
            }
            scriptPath('JenkinsFile')
        }
    }
    logRotator {
        numToKeep(15)
        artifactNumToKeep(15)
    }
}

//-----------------
pipelineJob('Containers/MySQL') {
    displayName('MySQL Container')
    description('\n This Job builds MySQL containers. \n --- This job is managed via JobDSL. --- \n --- DO NOT MANULLY MODIFY. ---')
    definition {
        cpsScm{
            lightweight(true)
            scm{
                git{
                    remote{
                        url('git@gitlab.com:currax_containers/MySQL.git')
                        credentials('curraxn_gitlab')
                    }
                    extensions{
                        wipeOutWorkspace()
                    }
                }
            }
            triggers {
                scm('H/5 * * * *')
            }
            scriptPath('JenkinsFile')
        }
    }
    logRotator {
        numToKeep(15)
        artifactNumToKeep(15)
    }
}
